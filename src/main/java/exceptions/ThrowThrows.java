package exceptions;

import java.io.IOException;

/**
 * Created by miro on 2019-02-22.
 */
public class ThrowThrows {

    public static void main(String[] args) {

        try {
            divideZero(5, 0);
        } catch (ArithmeticException ex) {
            System.out.println("wyjatek \"" + ex.toString() + "\"  zostal obsluzony");
        }catch (IOException ex){
            System.out.println("blad wejscia-wyjscia");
        }catch (Exception ex){
            System.out.println("dodatkowa ogsluga wyjatku");
        }

    }

    public static double divideZero(double a, double b) throws IOException{
        if (b == 0) {
//            throw new ArithmeticException("dzielenie przez 0");
            throw new IOException("dzielenie przez 0");
        } else {
            return a / b;
        }
    }
}
