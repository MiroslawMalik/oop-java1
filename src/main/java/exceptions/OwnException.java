package exceptions;

import org.omg.PortableServer.POAPackage.WrongAdapter;

/**
 * Created by miro on 2019-02-22.
 */
public class OwnException {
    public static void main(String[] args) {

        try{
            establishConnection(3);
        }catch (WrongPort ex){
            System.out.println(ex.toString());
        }

    }


    public static void establishConnection(int comName) throws WrongPort{
        if(comName<0){
            throw new WrongPort("Proba otwarcia portu nr "+comName);
        }else{
            System.out.println("Port nr "+comName+" zostal otwarty");
        }
    }
}


class WrongPort extends Exception{
    public WrongPort(String exception){
        super(exception);
    }
}