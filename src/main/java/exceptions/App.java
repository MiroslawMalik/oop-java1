package exceptions;

/**
 * Created by miro on 2019-02-19.
 */
public class App {

    public int counter = 0;


    public static void main(String[] args) {

        App app = new App();

        try {
            app.foo();
//        } catch (StackOverflowError exc) {
        } catch (NullPointerException exc) {
            System.out.println(app.toString() + "\tprzepelnienie stosu");
        }finally{
            System.out.println("\nwykonano dodatkowe operacje w finally\n");
        }

        System.out.println("\n...dalszy ciag programu");

    }// end main()


    public void foo() {

        System.out.println("ala ma kota  " + counter);
        counter++;
//        if(counter==100)return;
        foo();

    }

    @Override
    public String toString() {
        return "\nwystąpił błąd krytyczny: ---->>  ";
    }

}
